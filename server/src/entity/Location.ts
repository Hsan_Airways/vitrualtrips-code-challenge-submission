import { Column, Entity, Index, PrimaryColumn } from 'typeorm'

@Entity()
export class Location {
	@PrimaryColumn()
	geonameid: number

	@Column()
	@Index()
	name: string

	@Column()
	asciiname: string

	@Column()
	alternatenames: string

	@Column()
	latitude: number

	@Column()
	longitude: number

	@Column()
	feature_class: string

	@Column()
	feature_code: string

	@Column()
	country_code: string

	@Column()
	cc2: string

	@Column()
	admin1_code: string

	@Column()
	admin2_code: string

	@Column()
	admin3_code: string

	@Column()
	admin4_code: string

	@Column({ type: 'bigint' })
	population: string

	@Column()
	elevation: number

	@Column()
	dem: number

	@Column()
	timezone: string

	@Column()
	modification_date: string
}
