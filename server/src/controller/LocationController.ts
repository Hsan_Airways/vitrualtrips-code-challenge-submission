import { NextFunction, Request, Response } from 'express'

import { AppDataSource } from '../data-source'
import { ILike } from 'typeorm'
import { Location } from '../entity/Location'
import { validationResult } from 'express-validator'

export class LocationController {
	public async search(req: Request<{}, {}, {}, { q: string }>, res: Response, next: NextFunction) {
		const errors = validationResult(req)

		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() })
		}

		const q: string = req.query.q

		if (q.length < 2) {
			return res.status(200).json([])
		}

		const locations: Location[] = await AppDataSource.getRepository(Location)
			.createQueryBuilder('l')
			.where({ name: ILike(`${q}%`) })
			.orderBy('LENGTH(l.name)', 'ASC')
			.limit(10)
			.getMany()

		return res.status(200).send(locations.map((l) => l.name))
	}
}
