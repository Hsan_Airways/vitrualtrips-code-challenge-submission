# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

>This code will lead to the scheduling of the set timeout callback. Then the interperter will proceed to the next statement which prints out 2 and then idles after that. When the time specified in the set timeout is finished the interpeter executes the shceduled callback.

Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

>This is a recursive function that counts down from 10 to the initial value provided to the function. It only starts printing values when it has reached the halt condition. In this case the interpreter keeps adding calls with incremented d values to the stack and then once we hit the halt condition we start running the print statement of each function call and since the stack is LIFO (Last In First Out) the function calls are executed in this order from the last call that was in back to the first one.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

>The problem with this code is that we can land on the default value when we pass a falsy value like `0`

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

In JS, functions can be assigned to variables and still can be invoked. In this case foo is a function that returns another function which means bar is equivalent to another function that takes a `b` argument and adds `1` to it

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

>This function would be used by passing a callback function as the second parameter. To get the result we do `double(3, (result) => console.log(result))`
