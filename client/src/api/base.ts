import axios from 'axios'

export const BASE_URI = process.env.REACT_APP_BASE_URL

const api = axios.create({ baseURL: BASE_URI })

export default api
