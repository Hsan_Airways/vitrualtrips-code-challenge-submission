import { useEffect, useState } from 'react'

import { searchLocations } from '../api/search'

export function useSearchLocations(query: string) {
	const [noResults, setNoResults] = useState<boolean>(false)
	const [error, setError] = useState<boolean>(false)
	const [results, setResults] = useState<string[]>([])

	const fetchResults = async (query: string) => {
		setNoResults(false)
		setError(false)

		try {
			const locations = await searchLocations(query)
			setResults(locations)
			setNoResults(locations.length === 0)
		} catch (error) {
			setError(true)
			setResults([])
		}
	}

	useEffect(() => {
		if (query.length > 1) {
			fetchResults(query)
		} else {
			setResults([])
			setError(false)
			setNoResults(false)
		}
	}, [query])

	return {
		results,
		noResults,
		error,
	}
}
