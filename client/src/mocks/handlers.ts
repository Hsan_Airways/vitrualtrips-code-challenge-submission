import { rest } from 'msw'

export const handlers = [
	rest.get(`${process.env.REACT_APP_BASE_URL}/locations`, (req, res, ctx) => {
		return res(ctx.json(['Test1', 'Test2']))
	}),
]
